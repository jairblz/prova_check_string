<?php

declare(strict_types=1);
use PHPUnit\Framework\TestCase;
require_once('/var/www/html/prova_ileva/prova_check_string/src/colchete_prova.php');

// Para que todos testes sejam bem sucedidos é necessário tornar público as seguintes variáveis e funções:
// verificaCorrespondenciaColchete().
final class verificaCorrespondenciaColcheteTest extends TestCase
{

    public function testCorrespondencia() {

        $bstring = new StringRecebida('qualquer_uma');
        $flag = false;

        // Verifica se a correspondência de abertura e fechamento de "colchetes" está funcionando normalmente.
        $this->assertEquals($flag = $bstring->verificaCorrespondenciaColchete('(', ')'), false);
        $this->assertEquals($flag = $bstring->verificaCorrespondenciaColchete('[', ']'), false);
        $this->assertEquals($flag = $bstring->verificaCorrespondenciaColchete('{', '}'), false);
        $this->assertEquals($flag = $bstring->verificaCorrespondenciaColchete('(', '('), true);
        $this->assertEquals($flag = $bstring->verificaCorrespondenciaColchete('[', '['), true);
        $this->assertEquals($flag = $bstring->verificaCorrespondenciaColchete('{', '{'), true);
        $this->assertEquals($flag = $bstring->verificaCorrespondenciaColchete('(', '3'), true);
        $this->assertEquals($flag = $bstring->verificaCorrespondenciaColchete('(', '.'), true);
        $this->assertEquals($flag = $bstring->verificaCorrespondenciaColchete('(', '*'), true);
        $this->assertEquals($flag = $bstring->verificaCorrespondenciaColchete('(', '/'), true);
        $this->assertEquals($flag = $bstring->verificaCorrespondenciaColchete('[', '\''), true);
        $this->assertEquals($flag = $bstring->verificaCorrespondenciaColchete('{', ';'), true);

    }

}