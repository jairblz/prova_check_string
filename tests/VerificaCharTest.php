<?php

declare(strict_types=1);
use PHPUnit\Framework\TestCase;
require_once('/var/www/html/prova_ileva/prova_check_string/src/colchete_prova.php');

// Para que todos testes sejam bem sucedidos é necessário tornar público as seguintes variáveis e funções:
// verificaChar();
// getStringEntrada();
final class VerificaCharTest extends TestCase
{

    public function testVerificaChar() {

        $stringEntrada = 'Adedonhas';

        $nstring = new StringRecebida($stringEntrada);

        // Teste tanto do set como o get do array criado pela string.
        $this->assertEquals($nstring->getStringEntrada(), $stringEntrada);

        // Verificação do funcionamento da função verificaChar().
        $this->assertEquals($nstring->verificaChar('('), 1);
        $this->assertEquals($nstring->verificaChar('['), 1);
        $this->assertEquals($nstring->verificaChar('{'), 1);
        $this->assertEquals($nstring->verificaChar(')'), 2);
        $this->assertEquals($nstring->verificaChar(']'), 2);
        $this->assertEquals($nstring->verificaChar('}'), 2);
        $this->assertEquals($nstring->verificaChar('1'), 0);
        $this->assertEquals($nstring->verificaChar('n'), 0);
        $this->assertEquals($nstring->verificaChar('a'), 0);
        $this->assertEquals($nstring->verificaChar('.'), 0);
        $this->assertEquals($nstring->verificaChar(' '), 0);
        $this->assertEquals($nstring->verificaChar('/'), 0);
        $this->assertEquals($nstring->verificaChar('\''), 0);

    }

}