<?php

declare(strict_types=1);
use PHPUnit\Framework\TestCase;
require_once('/var/www/html/prova_ileva/prova_check_string/src/colchete_prova.php');

// Testa se a validação da string informada de acordo com os parâmetros desejados realmente está funcionando.
final class ValidaStringTest extends TestCase
{

    public function testValidacaoString() {

        $astring = new StringRecebida('(){}[](({}[[{{}}]]))({[((({{(())}})[])[])]})');
        $cstring = new StringRecebida('()()()');
        $dstring = new StringRecebida('[][][]');
        $estring = new StringRecebida('{}{}{}');
        $fstring = new StringRecebida('()');
        $gstring = new StringRecebida('[]');
        $hstring = new StringRecebida('{}');
        $istring = new StringRecebida('{}[()]');
        $jstring = new StringRecebida('a[]');
        $kstring = new StringRecebida('()b');
        $lstring = new StringRecebida('4{([]})[]');
        $mstring = new StringRecebida('(({}[]))5');
        $ostring = new StringRecebida('(([))');
        $pstring = new StringRecebida('([){]}');

        $this->assertEquals($astring->validaString(), true);
        $this->assertEquals($cstring->validaString(), true);
        $this->assertEquals($dstring->validaString(), true);
        $this->assertEquals($estring->validaString(), true);
        $this->assertEquals($fstring->validaString(), true);
        $this->assertEquals($gstring->validaString(), true);
        $this->assertEquals($hstring->validaString(), true);
        $this->assertEquals($istring->validaString(), true);
        $this->assertEquals($jstring->validaString(), false);
        $this->assertEquals($kstring->validaString(), false);
        $this->assertEquals($lstring->validaString(), false);
        $this->assertEquals($mstring->validaString(), false);
        $this->assertEquals($ostring->validaString(), false);
        $this->assertEquals($pstring->validaString(), false);

    }

}