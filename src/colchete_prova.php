<?php

class StringRecebida {

private $stringEntrada; // = '(){}[](({}[[{{}}]]))({[((({{(())}})[])[])]})'; // String a ser analisada.
private $arrayString; //= str_split($stringEntrada); // Array com os caracteres sequenciais da string a ser analisada.
private $delimitInicio = '/[(\{\[]/'; // Caracteres de abertura de um par de "colchetes".
private $delimitFim = '/[)\}\]]/'; // Caracteres de fechamento de um par de "colchetes".
private $arrayControle = []; // Array responsável por controlar/verificar a abertura e fechamento de "colchetes".
private $numElemControle = 0; // Número de elementos no array que controla a abertura e fechamento de "colchetes".
private $flagNoMatch = false; // Flag que caso esteja setada, sinaliza que a string a ser analisada não é válida para a análise em questão.

public function __construct($strEntrada) {
    $this->stringEntrada = $strEntrada;
    $this->arrayString = str_split($strEntrada);
}

// Retorna a string de entrada.
private function getStringEntrada() {

    return $this->stringEntrada;

}

// Retorna a variável $arrayString.
private function getArrayString() {

    return $this->arrayString;

}

// Retorna os caracteres que são de abertura de chaves, parênteses e colchetes.
private function getDelimitInicio() {

    return $this->delimitInicio;

}

// Retorna os caracteres que são de fechamento de chaves, parênteses e colchetes.
private function getDelimitFim() {

    return $this->delimitFim;

}

// Adiciona um item ao array $arrayControle.
private function pushArrayControle($item) {

    return array_push($this->arrayControle, $item);

}

// Remove o último item do $arrayControle.
private function popArrayControle() {

    return array_pop($this->arrayControle);

}

// Retorna o valor da variável $numElemControle.
private function getNumElemControle() {

    return $this->numElemControle;

}

// Aumenta em uma unidade a variável $numElemControle.
private function incrementaNumElemControle() {

    $this->numElemControle++;
    return;

}

// Decrementa em uma unidade a variável $numElemControle.
private function decrementaNumElemControle() {

    $this->numElemControle--;
    return;

}

// Retorna o valor da variável $flagNoMatch.
private function getFlag() {

    return $this->flagNoMatch;

}

// Ativa a flag indicadora de que a string não cumpre os requisitos necessários.
private function ativaFlag() {

    return $this->flagNoMatch = true;

}

// Verifica e retorna se o caractere analisado é um "colchete" de abertura, um "colchete" de fechamento ou um outro caractere qualquer.
// Valor retornado igual a 0, caso o caractere não seja de abertura e nem de fechamento de um colchete, chave ou parêntese.
// Valor retornado igual a 1, caso o caractere seja de abertura de colchete, chave ou parêntese.
// Valor retornado igual a 2, caso o caractere seja de fechamento de colchete, chave ou parêntese.
private function verificaChar($charParam) {

    if(preg_match($this->getDelimitInicio(), $charParam) == 1) {

        return 1;

    } elseif(preg_match($this->getDelimitFim(), $charParam) == 1) {

        return 2;

    } else {

        return 0;

    }

}

// Verifica se o atual caractere de fechamento de parênteses, chaves ou colchetes tem correspondência com o último caractere de abertura empilhado.
private function verificaCorrespondenciaColchete($elemento, $st) {

    if($elemento == '(' && $st != ')') {
    
        return $this->ativaFlag();

    } elseif($elemento == '[' && $st != ']') {

        return $this->ativaFlag();

    } elseif($elemento == '{' && $st != '}') {

        return $this->ativaFlag();

    }

    return false;

}

// Valida se a string informada é válida de acordo com os parâmetros desejados.
public function validaString() {

    foreach($this->getArrayString() as $s) {

        // Caso o caractere analisado seja de abertura de colchetes, chaves ou parênteses.
        if ($this->verificaChar($s) == 1) {
    
            $this->pushArrayControle($s); // Adiciona o caractere ao array de controle.
            $this->incrementaNumElemControle(); // Atualiza o número de elementos no array de controle.
    
        } elseif ($this->verificaChar($s) == 2) { // Caso o caractere analisado seja de fechamento de colchetes, chaves ou parênteses.
    
            // Caso o caractere de fechamento não tenha um caractere de abertura correspondente: entrará no if, sinalizará a incompatibilidade da string e sairá do loop.
            if ($this->getNumElemControle() == 0) {
                $this->ativaFlag();
                break;
            }
    
            $this->ultimoElementoControle = $this->arrayControle[$this->getNumElemControle()-1];

            // Se o "colchete" de fechamento não corresponde ao último "colchete" de abertura empilhado, então a string é avaliada como inválida.
            if($this->verificaCorrespondenciaColchete($this->ultimoElementoControle, $s)) {

                break;

            }
    
            $this->popArrayControle();
            $this->decrementaNumElemControle();
    
        } else { // Caso o caractere em análise não corresponda a abertura e nem fechamento de colchetes, chaves ou parênteses.
    
            $this->ativaFlag();
            break;
    
        }
    
    }

    return !$this->getFlag(); // Esta flag mostra verdadeiro para quando a string não é compatível com os requisitos. Desta forma, invertendo seu valor, conseguimos obter 
                              // verdadeiro para quando a string é compatível com os requisitos.

}

}

?>